## Overview

![Auto Deployment Process](https://svgshare.com/i/YaD.svg)

## NodeJS REST API

### Install Dependencies

Install the dependencies with the following command:

`npm install`

### Launch Development Server

To run a build and launch the development server, execute:

`npm start`

### Routes

Once completed, the REST API should be avialable at the following routes:

#### GET http://localhost:3000/info

Returns an example JSON response to confirm the API endpoint is accessible.

## Docker

This repo is configured with GitLab CI to build and deploy a docker image whenever changes are commited. The Docker image will be tagged with `latest` as well as the version listed in the `package.json`.

## GitLab Deployment

Changes will be deployed automatically to a configured Deployment Server (i.e. server being deployed to). For this to happen, ensure the following CI/CD variables are defined at either the group or project level in GitLab:

- `STAGE_SERVER_IP` - contains the IP address of the Deployment Server. This is the IP address used to make SSH connections from the GitLab Runner.
- `STAGE_SERVER_USER` - contains the user used when opening the SSH session.
- `STAGE_ID_RSA` - SSH private key used to authenticate when opening the SSH session.
